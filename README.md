CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Maintainers

INTRODUCTION
------------
The Search API OpenSearchServer module is a Search API backend for
OpenSearchServer.
 * For a full description of the module, visit the project page:
  https://drupal.org/project/search_api_opensearchserver
 * To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/search_api_opensearchserver
  
REQUIREMENTS
------------
This module requires the following modules:
 * Search API (https://drupal.org/project/search_api)
It requires a running OpenSearchServer isntance
 * OpenSearchServer (http://www.open-search-server.com)
  
INSTALLATION
------------
 * Install as usual, see
 https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.
  
MAINTAINERS
-----------
Current maintainer:
 * Emmanuel Keller (opensearchserver) - https://drupal.org/user/1272852
