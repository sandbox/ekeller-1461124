<?php
/**
 * @file
 * service.inc
 */

/**
 * Search service class using OpenSearchServer for storing index information.
 */
class SearchApiOpenSearchServerService extends SearchApiAbstractService {

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function configurationForm(array $form, array &$form_state) {
    if ($this->options) {
      $url = 'http://' . $this->options['host'] . ':' . $this->options['port'] . $this->options['path'];
      $form['server_description'] = array(
        '#type' => 'item',
        '#title' => t('OpenSearchServer server URI'),
        '#description' => l($url, $url));
    }
    $options = $this->options + array(
      'host' => 'localhost',
      'port' => '9090',
      'path' => '/',
      'login' => '',
      'api_key' => '');
    $form['host'] = array(
      '#type' => 'textfield',
      '#title' => t('OpenSearchServer host'),
      '#description' => t('The host name or IP of your OpenSearchServer server, e.g. <code>localhost</code> or <code>www.example.com</code>.'),
      '#default_value' => $options['host'],
      '#required' => TRUE);
    $form['port'] = array(
      '#type' => 'textfield',
      '#title' => t('OpenSearchServer port'),
      '#description' => t('The SaaS server is at port 80, while Tomcat uses 8080 by default.'),
      '#default_value' => $options['port'],
      '#required' => TRUE);
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('OpenSearchServer path'),
      '#description' => t('The path that identifies the OpenSearchServer instance to use on the server.'),
      '#default_value' => $options['path']);
    $form['login'] = array(
      '#type' => 'textfield',
      '#title' => t('OpenSearchServer Login'),
      '#default_value' => $options['login']);
    $form['api_key'] = array(
      '#type' => 'password',
      '#title' => t('OpenSearchServer Password'),
      '#default_value' => $options['api_key']);
    return $form;
  }

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
    if (isset($values['port']) && (!is_numeric($values['port']) || $values['port'] < 0 || $values['port'] > 65535)) {
      form_error($form['port'], t('The port has to be an integer between 0 and 65535.'));
    }
  }

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function supportsFeature($feature) {
    $supported = drupal_map_assoc(array(
      'search_api_autocomplete',
      'search_api_facets',
      'search_api_facets_operator_or',
      'search_api_mlt',
      'search_api_spellcheck'));
    return isset($supported[$feature]);
  }

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function addIndex(SearchApiIndex $index) {
    try {
      if (isset($index->machine_name)) {
        $this->removeIndex($index);
      }
      $oss_api = $this->getOssAPI();
      $oss_api->createIndex($index->machine_name);
      $oss_search_template = $this->getOssSearchTemplate($index->machine_name);
      $oss_search_template->createSearchTemplate('search', '$$', 'AND', '10', '2');
      $this->server->save();
    }
    catch (Exception $e) {
      watchdog('search_api_opensearchserver', check_plain($e->getMessage()), NULL, WATCHDOG_ERROR);
      throw $e;
    }
  }

  /**
   * Check if a string ends with a given suffix.
   *
   * @param string $haystack
   *   The challenge string.
   * @param string $needle
   *   The suffix to check.
   *
   * @return bool
   *   Return true if the string ends with the given suffix.
   */
  protected function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
      return TRUE;
    }
    return (substr($haystack, -$length) === $needle);
  }

  /**
   * Create the field on the OpenSearchServer instance.
   *
   * @param OssSchema $oss_schema
   *   An OSS schema instance.
   * @param SimpleXml $xml_schema
   *   The XML description of the schema.
   * @param string $field_name
   *   The name of the field to create.
   * @param string $analyzer
   *   The name of the analyzer.
   * @param string $stored
   *   The storage status.
   * @param string $indexed
   *   The indexation status
   * @param string $term_vector
   *   The term vector status
   * @param string $default
   *   The name of the default field.
   * @param string $unique
   *   The name of the unique field.
   */
  protected function setField(OssSchema $oss_schema, $xml_schema, $field_name, $analyzer, $stored, $indexed, $term_vector, $default = 'no', $unique = 'no') {
    $xml_field = $xml_schema->xpath('/response/schema/fields/field[@name="' . $field_name . '"]');
    // Check if the field already exists.
    if (isset($xml_field[0]) && is_array($xml_field)) {
      $oss_schema->deleteField($field_name);
    }
    $oss_schema->setField($field_name, $analyzer, $stored, $indexed, $term_vector, $default, $unique);
  }

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function fieldsUpdated(SearchApiIndex $index) {
    try {
      $oss_schema = $this->getOssSchema($index->machine_name);
      $fields = $index->getFields();
      // Load the schema from OpenSearchServer.
      $xml_schema = $oss_schema->getSchema();
      $this->setField($oss_schema, $xml_schema, 'id', '', 'no', 'yes', 'no', 'yes', 'yes');
      foreach ($fields as $name => $field) {
        $type = $field['type'];
        // Each type has specific parameters.
        switch ($type) {
          case 'tokens':
          case 'text':
            $this->setField($oss_schema, $xml_schema, $name, 'TextAnalyzer', 'yes', 'yes', 'positions_offsets');
            $this->setField($oss_schema, $xml_schema, $name . '_oss_exact', 'StandardAnalyzer', 'no', 'yes', 'no');
            break;

          case 'integer':
          case 'decimal':
          case 'date':
          case 'duration':
          case 'boolean':
          case 'uri':
          default:
            $this->setField($oss_schema, $xml_schema, $name, NULL, 'yes', 'yes', 'no');
            break;

        }
      }
      // Check which field has to be removed.
      $xml_fields = $xml_schema->xpath('/response/schema/fields/field');
      if (isset($xml_fields) && is_array($xml_fields)) {
        foreach ($xml_fields as $xml_field) {
          $name = (string) $xml_field->attributes()->name;
          if (isset($name)) {
            if (strcmp($name, 'id') == 0) {
              continue;
            }
            if (($this->endsWith($name, '_oss_exact'))) {
              $name = substr($name, 0, strlen($name) - 10);
            }
            if (!array_key_exists($name, $fields)) {
              $oss_schema->deleteField($name);
            }
          }
        }
      }
      $this->server->save();
      return TRUE;
    }
    catch (Exception $e) {
      watchdog('search_api_opensearchserver', check_plain($e->getMessage()), NULL, WATCHDOG_ERROR);
      throw $e;
    }
  }

  /**
   * Returns the URL of the OpenSearchServer instance.
   *
   * @return string
   *   The formatted URL: http://[host]:[port]/[path].
   */
  protected function getOssURI() {
    return 'http://' . $this->options['host'] . ':' . $this->options['port'] . $this->options['path'];
  }

  /**
   * Build a new OssAPI object.
   *
   * @param string $index_name
   *   The name of the index.
   *
   * @return OssApi
   *   A new OssAPi instance.
   */
  protected function getOssAPI($index_name = NULL) {
    if ($path = libraries_get_path('opensearchserver')) {
      require_once $path . '/oss_api.class.php';
      require_once $path . '/oss_indexdocument.class.php';
    }
    return new OssApi($this->getOssURI(), $index_name, $this->options['login'], $this->options['api_key']);
  }

  /**
   * Build a new OSSSearchTemplate object.
   *
   * @param string $index_name
   *   The name of the index.
   *
   * @return OSSSearchTemplate
   *   A new OSSSearchTemplate instance.
   */
  protected function getOssSearchTemplate($index_name = NULL) {
    if ($path = libraries_get_path('opensearchserver')) {
      require_once $path . '/oss_api.class.php';
      require_once $path . '/oss_searchtemplate.class.php';
    }
    return new OSSSearchTemplate($this->getOssURI(), $index_name, $this->options['login'], $this->options['api_key']);
  }

  /**
   * Returns a new OssResult object.
   *
   * @param SimpleXML $xml_result
   *   A simpleXML object.
   *
   * @return OssResult
   *   A new OssResult instance.
   */
  protected function getOssResults($xml_result) {
    if ($path = libraries_get_path('opensearchserver')) {
      require_once $path . '/oss_results.class.php';
    }
    return new OssResults($xml_result);
  }

  /**
   * Return a new OssSearch object.
   *
   * @param string $index_name
   *   The name of the index.
   *
   * @return OssSearch
   *   A new OssSearch instance.
   */
  protected function getOssSearch($index_name = NULL) {
    if ($path = libraries_get_path('opensearchserver')) {
      require_once $path . '/oss_api.class.php';
      require_once $path . '/oss_search.class.php';
    }
    return new OssSearch($this->getOssURI(), $index_name, NULL, NULL, $this->options['login'], $this->options['api_key']);
  }

  /**
   * Returns a new OssSchema object.
   *
   * @param string $index_name
   *   The name of the index.
   *
   * @return OssSchema
   *   A new OssSchema instance.
   */
  protected function getOssSchema($index_name) {
    if ($path = libraries_get_path('opensearchserver')) {
      require_once $path . '/oss_api.class.php';
    }
    return new OssSchema($this->getOssURI(), $index_name, $this->options['login'], $this->options['api_key']);
  }

  /**
   * Returns a new OssDelete object.
   *
   * @param string $index_name
   *   The name of the index
   *
   * @return OssDelete
   *   A new OssDelete instance.
   */
  protected function getOssDelete($index_name) {
    if ($path = libraries_get_path('opensearchserver')) {
      require_once $path . '/oss_api.class.php';
      require_once $path . '/oss_delete.class.php';
    }
    return new OssDelete($this->getOssURI(), $index_name, $this->options['login'], $this->options['api_key']);
  }

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function removeIndex($index) {
    try {
      $oss_api = $this->getOssAPI();
      // Delete only if it exists on OpenSearchServer.
      $index_list = $oss_api->indexList();
      if (in_array($index->machine_name, $index_list)) {
        $oss_api->deleteIndex($index->machine_name);
      }
    }
    catch (Exception $e) {
      watchdog('search_api_opensearchserver', check_plain($e->getMessage()), NULL, WATCHDOG_ERROR);
      throw $e;
    }
  }

  /**
   * Add a index field converting types.
   *
   * @param OSSIndexDocument $doc
   *   A document instance.
   * @param string $key
   *   The field name.
   * @param string $value
   *   The content of the field.
   * @param string $type
   *   The internal Drupal type of the field.
   */
  protected function addIndexField(OssIndexDocument_Document $doc, $key, $value, $type) {
    // Don't index empty values (i.e., when field is missing).
    if (!isset($value)) {
      return;
    }
    if (search_api_is_list_type($type)) {
      $type = substr($type, 5, -1);
      foreach ($value as $v) {
        $this->addIndexField($doc, $key, $v, $type);
      }
      return;
    }
    watchdog('search_api_opensearchserver', check_plain("INDEX " . $key . ' ' . $type . ' ' . var_export($value, TRUE)), NULL, WATCHDOG_NOTICE);
    switch ($type) {
      case 'tokens':
        foreach ($value as $v) {
          $this->addIndexField($doc, $key, $v['value'], 'text');
        }
        return;

      case 'boolean':
        $value = $value ? 1 : 0;
        break;

      case 'date':
        $value = is_numeric($value) ? (int) $value : $value;
        break;

      case 'integer':
        $value = (int) $value;
        break;

      case 'decimal':
        $value = (float) $value;
        break;

      case 'uri':
        $doc->newBinaryUrl($value, TRUE);
        break;
    }
    $doc->newField($key)
      ->addValues($value);
  }

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function indexItems(SearchApiIndex $index, array $items) {
    try {
      $indexed = array();
      $oss_api = $this->getOssAPI($index->machine_name);
      $doc_index = new OSSIndexDocument();
      foreach ($items as $id => $item) {
        // Create document (check lang).
        $lang = $item['search_api_language']['value'];
        if ($lang == 'und') {
          $lang = NULL;
        }
        $document = $doc_index->newDocument($lang);
        $this->addIndexField($document, 'id', $id, 'integer');
        // Set fields.
        foreach ($item as $key => $field) {
          $type = $field['type'];
          $this->addIndexField($document, $key, $field['value'], $type);
          if (search_api_is_text_type($type, array(
            'text',
            'tokens'))) {
            $this->addIndexField($document, $key . '_oss_exact', $field['value'], $type);
          }
        }
        $indexed[] = $id;
      }
      $oss_api->update($doc_index);
      return $indexed;
    }
    catch (Exception $e) {
      watchdog('search_api_opensearchserver', check_plain($e->getMessage()), NULL, WATCHDOG_ERROR);
      throw $e;
    }
  }

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function deleteItems($ids = 'all', SearchApiIndex $index = NULL) {
    try {
      $oss_delete = $this->getOssDelete($index->machine_name);
      if (is_array($ids)) {
        foreach ($ids as $id) {
          $oss_delete->delete('id:(' . $id . ')');
        }
      }
      else {
        $oss_delete->delete('*:*');
      }
    }
    catch (Exception $e) {
      watchdog('search_api_opensearchserver', check_plain($e->getMessage()), NULL, WATCHDOG_ERROR);
      throw $e;
    }
  }

  /**
   * Group terms.
   *
   * @param array $keys
   *   A list of keys.
   *
   * @return string
   *   A string which concat the keys.
   */
  protected function flattenKeys($keys) {
    $keywords = '';
    foreach ($keys as $key => $term) {
      if (is_int($key)) {
        $keywords = $keywords . $term . ' ';
      }
    }
    return trim($keywords);
  }

  /**
   * Espace colon characters.
   *
   * @param string $field
   *   An escaped string.
   */
  protected function escapeField($field) {
    return str_replace(':', '\:', $field);
  }

  /**
   * Create a filter query.
   *
   * @param string $field
   *   The name of the field.
   * @param string $value
   *   The text content of the field.
   * @param string $operator
   *   An operator.
   *
   * @return string
   *   An OpenSearchServer statement.
   */
  protected function createFilterQuery($field, $value, $operator) {
    $field = $this->escapeField($field);
    $value = trim($value);
    switch ($operator) {
      case '<>':
        return '*:* AND -' . $field . ':(' . $value . ')';

      case '<':
        return $field . ':{* TO ' . $value . '}';

      case '<=':
        return $field . ':[* TO ' . $value . ']';

      case '>=':
        return $field . ':[' . $value . ' TO *]';

      case '>':
        return $field . ':{' . $value . ' TO *}';

      default:
        return $field . ':(' . $value . ')';
    }
  }

  /**
   * Add facet information to the search query.
   *
   * @param OssSearch $oss_search
   *   The OpenSearchServer search instance.
   * @param array $index_fields
   *   The internal list of Drupal.
   * @param array $facets
   *   The internal list of Drupal.
   */
  protected function addFacets(OssSearch $oss_search, $index_fields, $facets) {
    foreach ($facets as $facet) {
      $field = $facet['field'];
      $multivalued = search_api_is_list_type($index_fields[$field]['type']);
      $min_count = $facet['min_count'];
      $oss_search->facet($this->escapeField($field), $min_count, $multivalued);
    }
  }

  /**
   * Add facet to the result.
   *
   * @param OssResults $oss_results
   *   The OpenSearchServer result instance.
   * @param array $facets
   *   The facet list from Drupal.
   * @param array $results
   *   The Drupal result structure.
   */
  protected function populateFacet(OssResults $oss_results, $facets, &$results) {
    foreach ($facets as $id => $facet) {
      $oss_facet_list = $oss_results->getFacet($facet['field']);
      if (isset($oss_facet_list)) {
        foreach ($oss_facet_list as $oss_facet_item) {
          $count = (int) $oss_facet_item;
          $term = (string) $oss_facet_item['name'][0];
          $results['search_api_facets'][$id][] = array(
            'count' => $count,
            'filter' => '"' . $term . '"');
        }
      }
    }
  }

  /**
   * Recursive function building the filter query.
   *
   * @param OssSearch $oss_search
   *   The OssSearch instance
   * @param unknown $filter
   *   The Filter instance
   */
  protected function createFilterCondition(OssSearch $oss_search, $filter, $returnValueInsteadOfFiltering = false) {
    if (!isset($filter)) {
      return;
    }
    $filterString = null;
    if (is_array($filter)) {
      $filterString = $this->createFilterQuery($filter[0], $filter[1], $filter[2]);
    }
    else if (is_object($filter)) {
      //OR: we need to build one string, we cannot use several filters that would only be "ANDed"
      if($filter->getConjunction() == 'OR') {
        $filtersOr = array();
        foreach ($filter->getFilters() as $f) {
          $filtersOr[] = $this->createFilterCondition($oss_search, $f, true);
        }
        //build the filter string by concatenating every filter. For ex: "type:article type:page"
        $filterString = implode(' ', $filtersOr);
      //AND: we build several filters to benefit from the OSS's field filter cache
      } else {
          foreach ($filter->getFilters() as $f) {
            $this->createFilterCondition($oss_search, $f);
          }
      }
    }
    //return filter string or apply filter to query
    if($returnValueInsteadOfFiltering) {
      return $filterString;
    } elseif($filterString) {
      $oss_search->filter($filterString);
    }
    return;
  }

  /**
   * Search API function.
   *
   * {@inheritdoc }
   */
  public function search(SearchApiQueryInterface $query) {
    $results = array();
    $results['result count'] = 0;
    $results['results'] = array();
    $ignored = array();
    $warnings = array();
    $time_method_called = microtime(TRUE);
    // Get query parameters.
    $index = $query->getIndex();
    $options = $query->getOptions();
    // Get an OSSSearch instance.
    $oss_search = $this->getOssSearch($index->machine_name);
    $oss_search->template('search');
    // Extract return fields.
    $fields = (isset($index->options['fields']) ? $index->options['fields'] : array());
    $oss_search->field('id');
    foreach ($fields as $key => $field) {
      $oss_search->field($key);
    }
    // Filters extration.
    $potentialFilter = $this->createFilterCondition($oss_search, $query->getFilter(), ($query->getFilter()->getConjunction() == 'OR'));
    if(!empty($potentialFilter)) {
        $oss_search->filter($potentialFilter);
    }
    // Get fields information.
    $index_fields = $index->getFields();
    $search_fields = $query->getFields();
    // Extract keywords.
    $keys = &$query->getKeys();
    if (is_array($keys)) {
      $keywords = $this->flattenKeys($keys);
    }
    else {
      $keywords = $keys;
    }
    // Return nothing if keywords is empty.
    if (!isset($keywords) || strlen(trim($keywords)) == 0) {
      return $results;
    }
    // Build the query.
    $q = '';
    foreach ($search_fields as $search_field) {
      if (strlen($q) > 0) {
        $q = $q . ' OR ';
      }
      $boost = $index_fields[$key]['boost'];
      $q .= $this->escapeField($search_field) . ':(' . $keywords . ')^' . $boost;
      if (search_api_is_text_type($fields[$search_field]['type'], array(
        'text',
        'tokens'))) {
        $q .= ' OR ' . $this->escapeField($search_field) . '_oss_exact:(' . $keywords . ')^' . $boost;
      }
    }
    $oss_search->query($q);
    // Pagination.
    $start = isset($options['offset']) ? $options['offset'] : 0;
    $rows = isset($options['limit']) ? $options['limit'] : 1000000;
    $oss_search->start($start);
    $oss_search->rows($rows);
    // Set conjunction AND/OR.
    $conjunction = $keys['#conjunction'];
    if (isset($conjunction)) {
      $oss_search->operator($conjunction);
    }
    // Prepare facets.
    $oss_facets = array();
    $oss_facets_or = array();
    // Get facets informations (split or and normal facets).
    if ($facets = $query->getOption('search_api_facets')) {
      foreach ($facets as $id => $facet) {
        $or = isset($facet['operator']) && $facet['operator'] == 'or';
        if ($or) {
          $oss_facets_or[$id] = $facet;
        }
        else {
          $oss_facets[$id] = $facet;
        }
      }
    }
    $this->addFacets($oss_search, $index_fields, $oss_facets);
    // Compute OR facets.
    if (count($oss_facets_or) > 0) {
      $oss_search_or = $this->getOssSearch($index->machine_name);
      $oss_search_or->template('search');
      $oss_search_or->query($q);
      $this->addFacets($oss_search_or, $index_fields, $oss_facets_or);
      $xml_result = $oss_search_or->execute();
      if (isset($xml_result) && $xml_result instanceof SimpleXMLElement) {
        $oss_results = $this->getOssResults($xml_result);
        $this->populateFacet($oss_results, $oss_facets_or, $results);
      }
    }
    // Applying sorting.
    $oss_sort = array();
    $sort = $query->getSort();
    if ($sort) {
      foreach ($sort as $field_name => $order) {
        if ($order != 'ASC' && $order != 'DESC') {
          $msg = t('Unknown sort order @order. Assuming "ASC".', array(
            '@order' => $order));
          $this->warnings[$msg] = $msg;
          $order = 'ASC';
        }
        $order = ($order == 'ASC') ? '' : '-';
        if ($field_name == 'search_api_relevance') {
          $oss_sort[] = $order . 'score';
          continue;
        }
        if ($field_name == 'search_api_id') {
          $oss_sort[] = $order . 'item_id';
          continue;
        }
        if (!isset($fields[$field_name])) {
          throw new SearchApiException(t('Trying to sort on unknown field @field.', array(
            '@field' => $field_name)));
        }
        $field = $fields[$field_name];
        $type = $field['type'];
        if (search_api_is_list_type($type)) {
          throw new SearchApiException(t('Cannot sort on field @field of a list type.', array(
            '@field' => $field_name)));
        }
        if (search_api_is_text_type($type)) {
          throw new SearchApiException(t('Cannot sort on fulltext field @field.', array(
            '@field' => $field_name)));
        }
        $oss_sort[] = $order . $this->escapeField($field_name);
      }
    } else {
      // Default sorting: score first, then "id", to keep order when navigating through pages
      // for documents having the same score.
      $oss_sort = array('score', 'id');
    }
    $oss_search->sort($oss_sort);
    // Execute the query.
    $time_processing_done = microtime(TRUE);
    $xml_result = $oss_search->execute();
    $time_queries_done = microtime(TRUE);
    // Prepare the result.
    if (isset($xml_result) && $xml_result instanceof SimpleXMLElement) {
      $oss_results = $this->getOssResults($xml_result);
      $results['result count'] = $oss_results->getResultFound();
      $max = $oss_results->getResultStart() + $oss_results->getResultRows();
      if ($max > $oss_results->getResultFound()) {
        $max = $oss_results->getResultFound();
      }
      // Result loop.
      for ($i = $oss_results->getResultStart(); $i < $max; $i++) {
        $id = (string) $oss_results->getField($i, 'id');
        $result = array(
          'id' => $id,
          'score' => (string) $oss_results->getScore($i),
          'fields' => array());
        foreach ($fields as $key => $field) {
          $result['fields'][$key] = (string) $oss_results->getField($i, $key);
        }
        $results['results'][$id] = $result;
      }
      $this->populateFacet($oss_results, $oss_facets, $results);
    }
    $results['warnings'] = array_keys($warnings);
    $results['ignored'] = array_keys($ignored);
    $time_end = microtime(TRUE);
    $results['performance'] = array(
      'complete' => $time_end - $time_method_called,
      'preprocessing' => $time_processing_done - $time_method_called,
      'execution' => $time_queries_done - $time_processing_done,
      'postprocessing' => $time_end - $time_queries_done);
    return $results;
  }
}
